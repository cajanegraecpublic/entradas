<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/custom-bootstrap-margin-padding.css" />
		<title>Resultado de la subida</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="mt-30">
						<?php
							require_once("conexion.php");
							session_start();
							$correo_generico = "info@wekickads.com";
							function validar_cedula($cedula) {
								//Preguntamos si la cedula consta de 10 digitos
								if (strlen($cedula) == 10 || strlen($cedula) == 13) {

									//Obtenemos el digito de la region que sonlos dos primeros digitos
									$digito_region = substr($cedula, 0, 2);

									//Pregunto si la region existe ecuador se divide en 24 regiones
								    if ($digito_region >= 1 && $digito_region <=24 ) {
								    	// Extraigo el ultimo digito
								    	$ultimo_digito = substr($cedula, 9, 1);

								    	//Agrupo todos los pares y los sumo
								    	$pares = intval(substr($cedula, 1, 1)) + intval(substr($cedula, 3, 1)) + intval(substr($cedula, 5, 1)) + intval(substr($cedula, 7, 1));

								    	//Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
								    	$numero1 = intval(substr($cedula, 0, 1));
								    	$numero1 = ($numero1 * 2);
								    	if ($numero1 > 9) {
								    		$numero1 = ($numero1 - 9);
								    	}

								    	$numero3 = intval(substr($cedula, 2, 1));
								    	$numero3 = ($numero3 * 2);
								    	if ($numero3 > 9) {
								    		$numero3 = ($numero3 - 9);
								    	}

								    	$numero5 = intval(substr($cedula, 4, 1));
								    	$numero5 = ($numero5 * 2);
								    	if ($numero5 > 9) {
								    		$numero5 = ($numero5 - 9);
								    	}

								    	$numero7 = intval(substr($cedula, 6, 1));
								    	$numero7 = ($numero7 * 2);
								    	if ($numero7 > 9) {
								    		$numero7 = ($numero7 - 9);
								    	}

								    	$numero9 = intval(substr($cedula, 8, 1));
								    	$numero9 = ($numero9 * 2);
								    	if ($numero9 > 9) {
								    		$numero9 = ($numero9 - 9);
								    	}

								    	$impares = $numero1 + $numero3 + $numero5 + $numero7 + $numero9;

								    	//Suma total
								    	$suma_total = ($pares + $impares);

								    	//extraemos el primero digito
								    	$primer_digito_suma = substr("" . $suma_total, 0, 1);

								    	//Obtenemos la decena inmediata
								    	$decena = (intval($primer_digito_suma) + 1) * 10;

								    	//Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
								    	$digito_validador = $decena - $suma_total;

								    	//Si el digito validador es = a 10 toma el valor de 0
								    	if ($digito_validador == 10) {
								    		$digito_validador = 0;
								    	}

								    	//Validamos que el digito validador sea igual al de la cedula
								    	if ($digito_validador == $ultimo_digito) {
								    		return true;
								    	} else {
								    		return false;
								    	}
								    } else {
								    	return false;
								    }
								} else {
									return false;
								}
							}
							if (isset($_POST['submit']) && isset($_FILES) && isset($_FILES['file']) && isset($_SESSION['verificacion']) && isset($_POST['csrf_token']) && $_SESSION['verificacion'] === $_POST['csrf_token']) {
								// vino el submit del form
								$estado = 1;
								if (isset($_POST['nombre_evento'])) {
									// se crea nuevo evento
									$nombre_evento = $_POST['nombre_evento'];
									if (strlen($nombre_evento) > 100) {
										$nombre_evento = substr($nombre_evento, 0, 100);
									}
									$nombre_evento = mysqli_real_escape_string($conexion, $nombre_evento);
									$query_evento = "INSERT INTO `eventos` (`nombre`) VALUES ('{$nombre_evento}')";
									// echo $query_evento;
									if (mysqli_query($conexion, $query_evento)) {
										$id_evento = mysqli_insert_id($conexion);
									} else {
										// echo "aqui";
										$estado = 0;
									}
								} elseif (isset($_POST['evento'])) {
									// se seleccionó evento por id
									$id_evento = intval($_POST['evento']);
								} else {
									// error
									$estado = 0;
								}

								if ($estado != 0) {
									$archivo = $_FILES['file']['name'];
									if (!is_dir("archivos/")) {
										mkdir("archivos/", 0777);
									}
									if (!is_dir("archivos/" . $id_evento . "/")) {
										mkdir("archivos/" . $id_evento . "/", 0777);
									}
									$filename = "archivos/" . $id_evento . "/" . date("Y-m-d-H-i-s") . ".xlsx";
									if ($archivo && move_uploaded_file($_FILES['file']['tmp_name'], "" . $filename)) {
										sleep(3);
										/** PHPExcel_IOFactory */
										require_once 'lib/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

										// echo date('H:i:s') , " Load from Excel2007 file" . "<br />";
										$objReader = PHPExcel_IOFactory::createReader('Excel2007');
										$objPHPExcel = $objReader->load($filename);

										// echo date('H:i:s') , " Iterate worksheets by Row" . "<br />";
										
										foreach ($objPHPExcel->getWorksheetIterator() as $ws_i => $worksheet) {
											if ($ws_i == 0) {
												// echo 'Worksheet - ' , $worksheet->getTitle() . "<br />";

												$query = "INSERT INTO `entradas` (`nombre`, `telefono`, `ciudad`, `correo`, `archivo`, `id_evento`, `estado`) VALUES ";
												foreach ($worksheet->getRowIterator() as $r_i => $row) {
													// echo '    Row number - ' , $row->getRowIndex() . "<br />";
													$cellIterator = $row->getCellIterator();
													$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
													foreach ($cellIterator as $c_i => $cell) {
														if ($r_i == 3) {
															if (($c_i == 'A' && $cell->getCalculatedValue() != "NOMBRE") || ($c_i == 'B' && $cell->getCalculatedValue() != "TELÉFONO") || ($c_i == 'C' && $cell->getCalculatedValue() != "CIUDAD") || ($c_i == 'D' && $cell->getCalculatedValue() != "CORREO")) {
																$estado = 0;
																break;
															}
														} elseif ($r_i > 3 && $c_i <= 'D') {
															if (!is_null($cell)) {
																// echo $cell->getCalculatedValue() . " | ";
																switch ($c_i) {
																	case 'A':
																		$nombre = $cell->getCalculatedValue();
																		break;
																	case 'B':
																		$telefono = $cell->getCalculatedValue();
																		break;
																	case 'C':
																		$ciudad = $cell->getCalculatedValue();
																		break;
																	case 'D':
																		$correo = trim($cell->getCalculatedValue());
																		break;
																	
																	default:
																		# code...
																		break;
																}
															}
															if ($c_i == 'D') {
																$correo = mysqli_real_escape_string($conexion, $correo);
																$id_evento = mysqli_real_escape_string($conexion, $id_evento);
																$querysel = "SELECT * FROM `entradas` WHERE `correo` = '{$correo}' AND `id_evento` = {$id_evento}";
																$rs = mysqli_query($conexion, $querysel);
																if ($rs) {
																	if ($correo != $correo_generico && mysqli_num_rows($rs) == 1) {
																		// echo "Correo repetido.";
																	} else {
																		if ($nombre && $correo && strlen($correo) > 10 && strlen($correo) < 50) {
																			$nombre = mysqli_real_escape_string($conexion, $nombre);
																			if ($telefono) {
																				$telefono = mysqli_real_escape_string($conexion, $telefono);
																			} else {
																				$telefono = "";
																			}
																			if ($ciudad) {
																				$ciudad = mysqli_real_escape_string($conexion, $ciudad);
																			} else {
																				$ciudad = "";
																			}
																			$correo = mysqli_real_escape_string($conexion, $correo);
																			$query .= "('{$nombre}', '{$telefono}', '{$ciudad}', '{$correo}', '{$filename}', {$id_evento}, 1), ";
																		}
																	}
																	mysqli_free_result($rs);
																}
															}
														}
														if ($estado == 0) {
															break;
														}
													}
													if ($estado == 0) {
														break;
													}
													// echo $r_i . "<br>";
												}
												$query = substr($query, 0, strlen($query) - 2);
												// echo $query . "<br><br>";
												if (!mysqli_query($conexion, $query)) {
													$estado = 0;
													break;
												} else {
													echo "Tabla ingresada exitosamente. Vuelva al <a href=\"index.php\">menú inicial</a>.";
												}
											} else {
												break;
											}
										}
										if ($estado == 0) {
											echo "Ha ocurrido un error. Vuelva a intentarlo ingresando a <a href=\"form.php\">este enlace</a>.";
										}
									} else {
										echo "Ha ocurrido un error. Vuelva a intentarlo ingresando a <a href=\"form.php\">este enlace</a>.";
									}
								} else {
									echo "Ha ocurrido un error. Vuelva a intentarlo ingresando a <a href=\"form.php\">este enlace</a>.";
								}
							} else {
								echo "Ha llegado aquí por error, diríjase a <a href=\"form.php\">este enlace</a>.";
							}
						?>
					</p>
				</div>
			</div>
		</div>
	</body>
</html>
<?php mysqli_close($conexion); ?>
<?php ob_flush(); ?>