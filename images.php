<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?php
require_once "conexion.php";
require_once "lib/php-barcode-master/barcode.php";
session_start();
if (isset($_POST['submit']) && isset($_FILES) && isset($_FILES['file']) && isset($_SESSION['verificacion']) && isset($_POST['csrf_token']) && $_SESSION['verificacion'] === $_POST['csrf_token'] && isset($_POST['evento'])) {
	$id_evento = $_POST['evento'];
	$id_evento = mysqli_real_escape_string($conexion, $id_evento);
	$querysel = "SELECT `id`, `nombre` FROM `entradas` WHERE `id_evento` = {$id_evento} AND `estado` = 1";
	$rs_entradas = mysqli_query($conexion, $querysel);
	if ($rs_entradas) {
		if (mysqli_num_rows($rs_entradas) > 0) {
			$cont = 0;
			$archivo = $_FILES['file']['name'];
			if (!is_dir("archivos/")) {
				mkdir("archivos/", 0777);
			}
			if (!is_dir("archivos/" . $id_evento . "/")) {
				mkdir("archivos/" . $id_evento . "/", 0777);
			}
			if (!is_dir("archivos/" . $id_evento . "/" . "imagenes/")) {
				mkdir("archivos/" . $id_evento . "/" . "imagenes/", 0777);
			}
			if (!is_dir("archivos/" . $id_evento . "/" . "imagenes/" . date("Y-m-d-H-i-s") . "/")) {
				mkdir("archivos/" . $id_evento . "/" . "imagenes/" . date("Y-m-d-H-i-s") . "/", 0777);
			}
			$dirname = "archivos/" . $id_evento . "/" . "imagenes/" . date("Y-m-d-H-i-s") . "/";
			$flag_png = false;
			if ($_FILES['file']['type'] == 'image/png') {
				$flag_png = true;
				$filename = $dirname . date("Y-m-d-H-i-s") . ".png";
			} else {
				$filename = $dirname . date("Y-m-d-H-i-s") . ".jpg";
			}
			if ($archivo && move_uploaded_file($_FILES['file']['tmp_name'], "" . $filename)) {
				sleep(3);
				while ($entrada = mysqli_fetch_assoc($rs_entradas)) {
					$cont++;
					if ($cont > 300) {
						break;
					}
					$codigo = str_pad($entrada['id'], 10, "0", STR_PAD_LEFT);
					
					if ($flag_png) {
						$dest = imagecreatefrompng($filename);
					} else {
						$dest = imagecreatefromjpeg($filename);
					}
					barcode( $dirname . "barcode.png", $codigo, "50", "horizontal", "code128", false, "1" );
					$src = imagecreatefrompng($dirname . 'barcode.png');


					//////////////////////////
					// Get image Width and Height
					$image_width = imagesx($dest);
					$image_height = imagesy($dest);

					$font_size = 10;
					$angle = 0;
					
					// Set Path to Font File
					$font_path = 'assets/fonts/Gotham-Bold.ttf';

					// Set Text to Be Printed On Image
					$text = $entrada['nombre'];

					// Get Bounding Box Size
					$text_box = imagettfbbox($font_size,$angle,$font_path,$text);

					// Get your Text Width and Height
					$text_width = $text_box[2]-$text_box[0];
					$text_height = $text_box[7]-$text_box[1];

					// Calculate coordinates of the text
					$x = ($image_width/2) - ($text_width/2);
					//$y = ($image_height/2) - ($text_height/2);
					$y = 105;


					// Allocate A Color For The Text
					$white = imagecolorallocate($dest, 255, 255, 255);

					// Print Text On Image
					imagettftext($dest, $font_size, $angle,  $x, $y, $white, $font_path, $text);

					//////////////////////////

					
					imagealphablending($dest, false);
					imagesavealpha($dest, true);
					
					imagecopymerge($dest, $src, 60, 360, 0, 0, 165, 50, 100); //have to play with these numbers for it to work for you, etc.
					
					// header('Content-Type: image/jpg');
					$generado = imagepng($dest, $dirname . $entrada['id'] . ".png");
					
					imagedestroy($dest);
					imagedestroy($src);

					if ($generado) {
						mysqli_query($conexion, "UPDATE `entradas` SET `estado` = 2 WHERE `id` = {$entrada['id']}");
					}
				}

				$zipname = $dirname . 'imagenes-' . date('Y-m-d-H-i-s') . '.zip';
			    $zip = new ZipArchive;
			    $resp = $zip->open($zipname, ZipArchive::CREATE);
			    if ($resp === TRUE) {
				    if ($handle = opendir($dirname)) {
				      while (false !== ($entry = readdir($handle))) {
				        if ($entry != "." && $entry != ".." && strstr($entry,'.png') && $entry != 'barcode.png') {
				            $resfile = $zip->addFile($dirname . $entry, $entry);
				        }
				      }
				      closedir($handle);
				    }
			    } else {
			    	echo $cont . " y no se creo";
			    	die();
			    }

			    $zip->close();
			    mysqli_free_result($rs_entradas);
				mysqli_close($conexion);

			    header('Content-Type: application/zip');
			    header("Content-Disposition: attachment; filename='" . $zipname . "'");
			    header('Content-Length: ' . filesize($zipname));
			    header("Location: " . $zipname );

				echo "Imágenes generadas y descargadas exitosamente. Vuelva al <a href=\"index.php\">menú inicial</a>.";
			} else {
			    mysqli_free_result($rs_entradas);
				echo "Ha ocurrido un error. Vuelva a intentarlo ingresando a <a href=\"imgform.php\">este enlace</a>.";
			}
		} else {
			mysqli_free_result($rs_entradas);
			echo "No hay entradas que imprimir. Escoja otro evento ingresando a <a href=\"imgform.php\">este enlace</a>. O ingrese más entradas <a href=\"form.php\">aquí.</a>";
		}
	} else {
		echo "Ha ocurrido un error. Vuelva a intentarlo ingresando a <a href=\"imgform.php\">este enlace</a>.";
	}
} else {
	echo "Ha llegado aquí por error, diríjase a <a href=\"imgform.php\">este enlace</a>.";
}





// // imagepng($dest);

// echo "<img src=\"lib/php-barcode-master/barcode.php?text=1\" alt=\"testing\" style=\"height: 80px;\" />";
?>
</body>
</html>