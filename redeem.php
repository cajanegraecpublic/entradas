<?php
	ob_start();
	session_start();
	require_once("conexion.php");
	function generateToken($length) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/custom-bootstrap-margin-padding.css" />
		<title>Validar entradas</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h2 class="">Validar entradas</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<!-- form id="redeemform" action="redeem.php" method="POST" class="form-inline" -->
						<div class="form-group">
							<label for="identificador">Identificador</label>
							<input type="text" id="identificador" name="identificador" />
						</div>
						<!-- input type="submit" name="submit" class="btn btn-primary" / -->
						<?php
							$token = generateToken(20);
							$_SESSION['verificacion'] = $token;
						?>
						<?php echo "<input id=\"csrf_token\" required type=\"text\" style=\"display: none;\" name=\"csrf_token\" value=\"" . $token . "\" />"; ?>
					<!-- /form -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div id="redeemdiv"></div>
				</div>
			</div>
		</div>
</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
<!-- <script type="text/javascript" src="assets/js/popper.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script> -->
<script type="text/javascript">
	$(document).ready(function() {
		$("#identificador").keydown(function(event) {
			// console.log('"' + event.keyCode + '"\n');
			if (event.which == 17 || event.which == 74 || event.keyCode == 13) {
				event.preventDefault();
			}
			var identificador = $("#identificador").val();
			if (identificador.length == 10) {
				$("#identificador").val("");
				$.ajax({
					url: 'redeemprocess.php',
					type: 'POST',
					data: {identificador: identificador, submit: true, csrf_token: $("#csrf_token").val()}
				})
				.done(function(data) {
					$("#redeemdiv").prepend("<p style=\"color: " + data.color + ";\">" + data.nombre + " --- " + data.codigo + "</p>");
				})
				.fail(function() {
					alert("Ha ocurrido un error");
				});
			}
		});
	});
</script>
</html>
<?php mysqli_close($conexion); ?>
<?php ob_flush(); ?>