<?php
ob_start();
session_start();
function generateToken($length) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/custom-bootstrap-margin-padding.css" />
		<title>Entradas</title>
	</head>
	<body>
		<?php
		$token = generateToken(20);
		$_SESSION['verificacion'] = $token;
		?>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h2 class="">Generador de entradas a Evento</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-8">
					<div class="btn-group">
						<a href="form.php" class="btn btn-primary">Ingreso de Excel</a>
						<a href="imgform.php" class="btn btn-success">Exportar imágenes</a>
						<a href="redeem.php" class="btn btn-info">Validar entradas</a>
						<a href="reporte.php" class="btn btn-warning">Reporte de asistencia</a>
						<button id="btn_eliminar" type="button" data-ruta="eliminar.php" class="btn btn-danger" <?php echo "data-token=\"" . $token . "\"" ?>>Eliminar registros</button>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

		<script>
			$(document).ready(function() {
				$("#btn_eliminar").click(function(event) {
					var boton = $(this);
					var confirmacion = confirm("¿Seguro que desea eliminar todos los registros? Esta acción no se puede deshacer.");
					if (confirmacion) {
						$.ajax({
							url: boton.attr("data-ruta"),
							type: 'POST',
							data: {csrf_token: boton.attr("data-token")},
						})
						.done(function(data) {
							if (data.codigo == 1) {
								alert("Registros eliminados exitosamente.");
							} else {
								alert("Ha ocurrido un error al eliminar registros.");
							}
						})
						.fail(function() {
							alert("Ha ocurrido un error al eliminar registros.");
						});
					}
				});
			});
		</script>
	</body>
</html>
<?php ob_flush(); ?>