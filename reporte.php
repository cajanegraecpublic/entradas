<?php
ob_start();
session_start();
require_once("conexion.php");
?>

<?php
$data = [];
if (isset($_POST['submit'])) {
	$query = "SELECT en.*, ev.`nombre` AS evento FROM `entradas` AS en JOIN `eventos` AS ev ON en.`id_evento` = ev.`id` WHERE 1 ";
	if (isset($_POST['evento']) && $_POST['evento'] != "") {
		$evento = mysqli_real_escape_string($conexion, $_POST['evento']);
		$query .= "AND en.`id_evento` = {$evento} ";
	}
	if (isset($_POST['estado']) && $_POST['estado'] != "") {
		$estado = mysqli_real_escape_string($conexion, $_POST['estado']);
		$query .= "AND en.`estado` = {$estado} ";
	}
	if (isset($_POST['fecha']) && $_POST['fecha'] != "") {
		$fecha = mysqli_real_escape_string($conexion, $_POST['fecha']);
		$query .= "AND en.`fecha` = '{$fecha}' ";
	}
	$query .= "ORDER BY en.`id_evento` ASC";
	$entradas = mysqli_query($conexion, $query);
	if ($entradas) {
		if (mysqli_num_rows($entradas) > 0) {
			while ($entrada = mysqli_fetch_assoc($entradas)) {
				$dato_entrada = array();
				$dato_entrada["nombre"] = $entrada["nombre"];
				$dato_entrada["correo"] = $entrada["correo"];
				$dato_entrada["celular"] = str_pad($entrada["telefono"], 10, "0", STR_PAD_LEFT);
				$dato_entrada["ciudad"] = $entrada["ciudad"];
				$dato_entrada["evento"] = $entrada["evento"];
				switch ($entrada["estado"]) {
					case 1:
						$dato_entrada["estado"] = "Ingresado";
						break;
					case 2:
						$dato_entrada["estado"] = "Entrada generada";
						break;
					case 3:
						$dato_entrada["estado"] = "Asistió";
						break;
					
					default:
						$dato_entrada["estado"] = "N/D";
						break;
				}
				$dato_entrada["fecha"] = $entrada["fecha"];
				$data[]= $dato_entrada;
			}
			mysqli_free_result($entradas);
		} else {
			$data = 0;
		}
	} else {
		$data = 0;
	}
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/custom-bootstrap-margin-padding.css" />
		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
		<title>Reporte de Asistencia</title>
	</head>
	<body>
		<!-- #page-content-wrapper -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="index-box">
                            <h1 class="mt-10">Reporte de Asistencia</h1>
                        </div>
                    </div>
                </div>
                <div id="filtros" class="row mt-10 mb-10">
                	<form action="reporte.php" method="POST">
	                	<div class="col-xs-12 col-sm-6 col-md-3">
	                		<label for="evento">Evento: </label>
	                		<select name="evento" id="evento">
	                			<option value="">Todos</option>
	                			<?php
	                			$eventos = mysqli_query($conexion, "SELECT * FROM `eventos` WHERE `estado` = 1");
	                			if ($eventos) {
	                				while ($evento = mysqli_fetch_assoc($eventos)) {
	                					$string = "<option value=\"{$evento['id']}\" ";
	                					if ($_POST["evento"] == $evento['id']) {
	                						$string .= "selected";
	                					}
	                					$string .= ">{$evento['nombre']}</option>";
	                					echo $string;
	                				}
	                				mysqli_free_result($eventos);
	                			}
	                			?>
	                		</select>
	                	</div>
	                	<div class="col-xs-12 col-sm-6 col-md-3">
	                		<label for="estado">Estado: </label>
	                		<select name="estado" id="estado">
	                			<option value="">Todos</option>
	                			<option value="1" <?php if ($_POST['estado'] == 1) { echo "selected"; } ?> >Ingresado</option>
	                			<option value="2" <?php if ($_POST['estado'] == 2) { echo "selected"; } ?> >Entrada generada</option>
	                			<option value="3" <?php if ($_POST['estado'] == 3) { echo "selected"; } ?> >Asistió</option>
	                		</select>
	                	</div>
	                	<div class="col-xs-12 col-sm-6 col-md-3">
	                		<label for="fecha">Fecha: </label>
	                		<input type="date" id="fecha" name="fecha" value="<?php if (isset($_POST['fecha']) && $_POST['fecha'] != "") { echo $_POST['fecha']; } ?>" />
	                	</div>
	                	<div class="col-xs-12 col-sm-6 col-md-3">
	                		<button name="submit" type="submit" id="btnFiltrar">Filtrar</button>
	                	</div>
                	</form>
                </div>
                <div class="row mt-10 mb-10">
                    <div class="col-md-12 text-center">
                        <table class="table table-hover" id="tablaAprobados">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Correo</th>
                                    <th>Celular</th>
                                    <th>Ciudad</th>
                                    <th>Evento</th>
                                    <th>Estado</th>
                                    <th>Fecha de Asistencia</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php if (isset($_POST["submit"]) && $data != 0 && $data != []): ?>
                            		<?php foreach ($data as $index => $entrada): ?>
	                            		<tr>
		                            		<td><?php echo ($index + 1); ?></td>
											<td><?php echo $entrada["nombre"]; ?></td>
											<td><?php echo $entrada["correo"]; ?></td>
											<td><?php echo $entrada["celular"]; ?></td>
											<td><?php echo $entrada["ciudad"]; ?></td>
											<td><?php echo $entrada["evento"]; ?></td>
											<td><?php echo $entrada["estado"]; ?></td>
											<td><?php echo $entrada["fecha"]; ?></td>
	                            		</tr>
                            		<?php endforeach; ?>
                            	<?php else: ?>
                            		<tr><td colspan="8">No hay datos qué presentar.</td></tr>
                            	<?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-content-wrapper -->
	</body>
	<!-- Optional JavaScript -->
	<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
	<script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tablaAprobados').DataTable({
			    dom: 'Bfrtip',
		        buttons: [
		            'csv', 'excel'
		        ]
			});
			$("#btnFiltrar").click(function(event) {
				$.ajax({
					url: 'obtenerReporte.php',
					method: 'POST',
					data: {evento: $("#evento").val(), estado: $("#estado").val(), fecha: $("#fecha").val()}
				})
				.done(function(data) {
					var str = "";
					if (data.codigo == 1) {
						$.each(data.data, function(index, elem) {
							str += "<tr>";
							str += "<td>" + (index + 1) + "</td>";
							str += "<td>" + elem.nombre + "</td>";
							str += "<td>" + elem.correo + "</td>";
							str += "<td>" + elem.celular + "</td>";
							str += "<td>" + elem.ciudad + "</td>";
							str += "<td>" + elem.evento + "</td>";
							str += "<td>" + elem.estado + "</td>";
							str += "<td>" + elem.fecha + "</td>";
							str += "</tr>";
						});
					} else {
						str = "<tr><td colspan=\"8\">No hay datos qué presentar.</td></tr>";
					}
					$("#tablaAprobados tbody").html(str);
				})
				.fail(function() {
					console.log("error");
					str = "<tr><td colspan=\"8\">No hay datos qué presentar.</td></tr>";
					$("#tablaAprobados tbody").html(str);
				});
			});
		});
	</script>
</html>
<?php
mysqli_close($conexion);
ob_flush();
?>