<?php
ob_start();
session_start();
require_once("conexion.php");

$resultado = array("codigo" => 0);
if (isset($_POST['csrf_token']) && isset($_SESSION['verificacion']) && $_POST['csrf_token'] == $_SESSION['verificacion']) {
	$query1 = "DELETE FROM `entradas` WHERE 1";
	$query2 = "DELETE FROM `eventos` WHERE 1";

	$result1 = mysqli_query($conexion, $query1);
	$result2 = mysqli_query($conexion, $query2);

	if ($result1 && $result2) {
		$resultado['codigo'] = 1;
	}
}

header('Content-type: application/json');
echo json_encode($resultado);
mysqli_close($conexion);
ob_flush();
?>