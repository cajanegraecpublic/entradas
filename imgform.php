<?php
	ob_start();
	session_start();
	require_once("conexion.php");
	function generateToken($length) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/custom-bootstrap-margin-padding.css" />
		<title>Generar Imagen</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<h2 class="">Generar Imágenes</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">

					<form action="images.php" method="post" enctype="multipart/form-data" class="form-horizontal">
						<div class="form-group">
							<label for="evento_select" class="col-sm-4 control-label">
								Seleccionar evento:
							</label>
							<div class="col-sm-8">
								<select name="evento" id="evento_select" required class="form-control">
									<?php
										$query = "SELECT * FROM `eventos` ORDER BY `id` DESC";
										$rs_eventos = mysqli_query($conexion, $query);
										if ($rs_eventos) {
											$num_eventos = mysqli_num_rows($rs_eventos);
											if ($num_eventos == 0) {
												echo "<option value=\"\">No existen eventos.</option>";
											} else {
												echo "<option value=\"\">Escoja una opción.</option>";
												while ($evento = mysqli_fetch_assoc($rs_eventos)) {
													echo "<option value=\"{$evento['id']}\">{$evento['nombre']}</option>";
												}
											}
											mysqli_free_result($rs_eventos);
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="file" class="col-sm-4 control-label">
								Seleccionar plantilla de entradas (283x463 px):
							</label>
							<div class="col-sm-8">
								<input type="file" name="file" class="btn btn-default" required />
							</div>
						</div>
						<div class="form-group">
						    <div class="col-sm-offset-4 col-sm-8">
						    	<a href="index.php" class="btn btn-default">Regresar</a>
								<input type="submit" name="submit" class="btn btn-success" />
						    </div>
						</div>
						<?php
							$token = generateToken(20);
							$_SESSION['verificacion'] = $token;
						?>
						<?php echo "<input required type=\"text\" style=\"display: none;\" name=\"csrf_token\" value=\"" . $token . "\" />"; ?>
					</form>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	</body>
</html>
<?php mysqli_close($conexion); ?>