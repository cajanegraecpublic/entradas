<?php
ob_start();
session_start();
require_once("conexion.php");
if (isset($_POST['submit']) && isset($_POST['identificador']) && isset($_SESSION['verificacion']) && isset($_POST['csrf_token']) && $_SESSION['verificacion'] === $_POST['csrf_token']) {
	$identificador = intval(mysqli_real_escape_string($conexion, $_POST['identificador']));
	$query = "SELECT * FROM `entradas` WHERE `id` = {$identificador}";
	$rs = mysqli_query($conexion, $query);
	if ($rs) {
		$data = array();
		if ($entrada = mysqli_fetch_assoc($rs)) {
			// existe
			$data['nombre'] = $entrada['nombre'];
			if ($entrada['estado'] == 1) {
				// error - imagen no generada
				$data['color'] = "red";
				$data['codigo'] = "ERROR: No se generó imagen para este usuario.";
			} elseif ($entrada['estado'] == 2) {
				// exito
				$data['color'] = "green";
				$data['codigo'] = "APROBADO.";
				mysqli_query($conexion, "UPDATE `entradas` SET `estado` = 3, `fecha` = '" . date("Y-m-d") . "' WHERE `id` = {$identificador}");
			} elseif ($entrada['estado'] == 3) {
				// error - codigo ya escaneado
				$data['color'] = "red";
				$data['codigo'] = "ERROR: Este usuario ya fue escaneado.";
			} else {
				// error
				$data['color'] = "red";
				$data['codigo'] = "ERROR: " . $entrada['estado'] . ". Comunicarse con servicio técnico.";
			}
		} else {
			// no existe
			$data['color'] = "red";
			$data['nombre'] = "N/D";
			$data['codigo'] = "N/D";
		}
		header('Content-type: application/json');
		echo json_encode($data);
		mysqli_free_result($rs);
	} else {
		error_reporting(0);
	}
} else {
	error_reporting(0);
}
mysqli_close($conexion);
ob_flush();
?>